import 'package:flutter/material.dart';
import 'package:migo_test/components/constants.dart';
import 'package:migo_test/pages/about_me.dart';
import 'package:migo_test/pages/gallery.dart';

class GlobalDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final drawerHeader = UserAccountsDrawerHeader(
      accountName: const Text(
        'Игорь Ожиганов',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      accountEmail: const Text('confirmedancient@mail.ru'),
      currentAccountPicture:
          CircleAvatar(backgroundImage: NetworkImage(my_img_url)),
    );

    final drawerItems = ListView(
      children: [
        drawerHeader,
        ListTile(
          leading: const Icon(Icons.folder),
          title: const Text('Галерея'),
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => GalleryPage(),
            ),
          ),
        ),
        ListTile(
          leading: const Icon(Icons.person),
          title: const Text('Обо мне'),
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => AboutMePage(),
            ),
          ),
        ),
      ],
    );
    return Drawer(
      child: drawerItems,
    );
  }
}

PreferredSizeWidget globalAppBar(String text) {
  return AppBar(title: Text(text));
}
