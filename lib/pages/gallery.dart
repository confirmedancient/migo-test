import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:migo_test/bloc/picsBloc/pics_bloc.dart';
import 'package:migo_test/bloc/picsBloc/pics_event.dart';
import 'package:migo_test/bloc/picsBloc/pics_state.dart';
import 'package:migo_test/components/constants.dart';
import 'package:migo_test/models/pic.dart';
import 'package:migo_test/widgets/category_card.dart';
import 'package:migo_test/widgets/global_header.dart';

class GalleryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PicsBloc picsBloc = BlocProvider.of<PicsBloc>(context);
    picsBloc.add(PicsLoadEvent());

    return Scaffold(
      appBar: globalAppBar('Галерея'),
      drawer: GlobalDrawer(),
      body: BlocBuilder<PicsBloc, PicsState>(
        builder: (context, state) {
          if (state is PicsLoadingState) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          } else if (state is PicsLoadedState) {
            return ListView.builder(
              itemCount: num_categories,
              itemBuilder: (context, i) {
                final List<Pic> pics = state.pics
                    .getRange(i * num_pics, (i + 1) * num_pics)
                    .toList();
                return CategoryCard(pics: pics, categoryId: i + 1);
              },
            );
          } else if (state is PicsLoadedState) {
            return const Center(child: Text('Не удалось загрузить данные'));
          }
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        },
      ),
    );
  }
}
