import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:migo_test/bloc/picBloc/pic_bloc.dart';
import 'package:migo_test/bloc/picsBloc/pics_bloc.dart';
import 'package:migo_test/pages/gallery.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => PicsBloc()),
        BlocProvider(create: (context) => PicBloc()),
      ],
      child: MaterialApp(
        title: 'Migo test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: GalleryPage(),
      ),
    );
  }
}
